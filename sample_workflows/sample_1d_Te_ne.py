#!/usr/bin/env python

import sys
import os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..')) 

import numpy as np
from uncertainties import unumpy as unp
import matplotlib.pyplot as plt; plt.ion()

from ida.core.ida import IDA
from ida.core.variables import Independent1DVariable, CubicSplineDependent1DVariable
from ida.core.likelihoods import GaussianLikelihood 
from ida.core.priors import Positivity1DPrior
from ida.diagnostics.sample.ECE import SimpleElectronCyclotronEmission
from ida.diagnostics.sample.TS import SimpleThomsonScattering


rp = Independent1DVariable('rho_pol', (0, 1.2))

knots = np.array([0, 0.5, 0.9, 0.95, 1, 1.05, 1.2])

Te = CubicSplineDependent1DVariable('Te', rp, knots)
ne = CubicSplineDependent1DVariable('ne', rp, knots)

# an initial guess to shorten the optimisation:
Te.set_parameters(1e3*np.linspace(1,0,len(knots))**2)
ne.set_parameters(6e19*np.linspace(1,0,len(knots))**2)

ida = IDA()
ida.add_independent_variable(rp)
ida.add_dependent_variable(Te)
ida.add_dependent_variable(ne)

ece = SimpleElectronCyclotronEmission(
    rho_pol=rp,
    Te=Te,
    likelihood=GaussianLikelihood())

ts = SimpleThomsonScattering(
    rho_pol=rp,
    Te=Te,
    ne=ne,
    likelihood=GaussianLikelihood())

pp = Positivity1DPrior(states=[Te, ne])

ida.add_diagnostic(ece)
ida.add_diagnostic(ts)
ida.add_prior(pp)

ida.prepare() # without time_interval here since no time dependency in sample

res = ida.optimize() 


# plotting prep
rhos = np.linspace(0, 1.2, 100)

plt.figure(1)
plt.clf()
ax1 = plt.subplot(211)
ax2 = plt.subplot(212)
ax1.grid()
ax2.grid()

# plot fit results
Tes = ida.get_dependent_variable("Te").evaluate(rhos)
ax1.plot(rhos, Tes, alpha=1)

nes = ida.get_dependent_variable("ne").evaluate(rhos)
ax2.plot(rhos, nes, alpha=1)

# plot sample TS
tsr, tsd = ts.get_data(apply_ne_norm=False)
tste, tsne = tsd[0]
tsr = tsr[0]
ax1.errorbar(tsr, unp.nominal_values(tste), unp.std_devs(tste))
ax2.errorbar(tsr, unp.nominal_values(tsne), unp.std_devs(tsne))

# plot sample ECE
rps, data = ece.get_data()
for rp, d in zip(rps, data):
    Te_m_, Te_s_ = unp.nominal_values(d), unp.std_devs(d)
    ax1.errorbar(rp, Te_m_, Te_s_, ls='--', alpha=0.9)
    break

plt.show()


sys.exit()
