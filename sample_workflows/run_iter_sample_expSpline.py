#!/usr/bin/env python

import sys
import os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))

import numpy as np
from uncertainties import unumpy as unp
import matplotlib.pyplot as plt
try:
    plt.switch_backend('TkAgg')#; plt.ion()
except Exception as Error:
    print("Could not switch to TkAgg: {}".format(Error))
from scipy.interpolate import RegularGridInterpolator, CubicSpline, interp1d

from ida.core.ida import IDA
from ida.core.variables import Independent1DVariable, CubicSplineDependent1DVariable
from ida.core.variables import UnivSplineDependent1DVariable, ExpUnivSplineDependent1DVariable
from ida.core.likelihoods import GaussianLikelihood, CauchyLikelihood
from ida.core.priors import Positivity1DPrior
from ida.diagnostics.sample.ECE import SimpleElectronCyclotronEmission
from ida.diagnostics.sample.TS import SimpleThomsonScattering
from ida.diagnostics.iter.TIP import SimpleInterferometry
from ida.diagnostics.iter.iter_equilibrium import ITER2DEquilibrium


###############################################################
# ADDITIONAL CLASS FOR DATABASE IDENTIFIER
###############################################################
class DataBaseIdentifier:
    def __init__(self):
        from datetime import datetime
        import os

        #self.equilibrium_experiment      = 'ITER'    # for ITER lokin nodes
        self.equilibrium_experiment      = 'iterdb'  # for tok0X nodes
        self.equilibrium_pulse_number    = 100003
        self.equilibrium_edition         = 1
        self.equilibrium_input_user      = 'public'

        #self.interferometry_experiment   = 'ITER'
        self.interferometry_experiment   = self.equilibrium_experiment
        self.interferometry_pulse_number = self.equilibrium_pulse_number
        self.interferometry_edition      = 1
        self.interferometry_input_user   = 'public'

        self.output_experiment           = 'iter'
        self.user                        = os.getenv("USER")                # user running the code

        self.imas_version                = os.getenv('IMAS_VERSION')[0]     # IMAS DATA VERSION

        # CURRENT DATE
        now                   = datetime.now()
        self.today            = now.strftime("%y-%m-%d")
        self.input_param_file = "iter_parameters.xml"


#rp = np.linspace(0,1.2, 100)
rp = Independent1DVariable('rho_pol', (0, 1.2))

#knots = np.array([0, 0.5, 0.9, 0.95, 1, 1.05, 1.2])
knots = np.array([0, 0.5, 0.8, 0.9, 1, 1.10, 1.2])
#knots = np.array([0, 0.2, 0.4, 0.6, 0.8, 0.9, 0.95, 1, 1.09, 1.2])
#knots = np.array([0, 1, 1.2])

#basis_functions = []
#for cv in np.eye(len(knots)):
#    func = CubicSpline(knots, cv, bc_type=('clamped','clamped'), extrapolate=True)
#    # func = interp1d(rp, np.exp(CubicSpline(knots, cv, bc_type=('clamped','clamped'), extrapolate=True)(rp)),
#    #        bounds_error=False, fill_value="extrapolate")
#    basis_functions.append(func)
if 0:  # standard cubic spline, sum over once initialised basis
    print("Using standard cubic spline")
    model = CubicSplineDependent1DVariable
elif 1:  # standard cubic spline, initialise for every call
    print("Using standard cubic spline")
    model = UnivSplineDependent1DVariable
elif 0:  # spline(exp(par)) ?
    print("Test for spline(exp(par))")
    model = UnivSplineDependent1DVariable
else:
    print("Test for exp(spline)")
    model = ExpUnivSplineDependent1DVariable
Te = model('Te', rp, knots)
ne = model('ne', rp, knots)

# an initial guess to shorten the optimisation:
Te.set_parameters(1e3*np.linspace(1,0,len(knots))**2)
ne.set_parameters(6e19*np.linspace(1,0,len(knots))**2)
if model.__name__ in ['ExpCubicSplineDependent1DVariable', 'ExpUnivSplineDependent1DVariable']:
    # adapted scale when using something on the lines of exp(par)
    Te.set_parameters(np.log(1e3*np.linspace(1,0,len(knots))**2 + 1e0))
    ne.set_parameters(np.log(6e19*np.linspace(1,0,len(knots))**2 + 1e16))
print("Initial parameter sets")
print("  Te")
print(Te.get_parameters())
print("  ne")
print(ne.get_parameters())
print("------------------------")

ida = IDA()
ida.add_independent_variable(rp)
ida.add_dependent_variable(Te)
ida.add_dependent_variable(ne)
#ida.add_independent_variable("rp", rp)
#ida.add_dependent_variable("Te", "rp", basis_functions,
#    initial_weights=3e3*np.linspace(1,0,len(basis_functions))**2,
#    #initial_weights=3e3*np.array([1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3, 0.1, .005, 0]),
#    initial_std_devs=1e2*np.ones(len(basis_functions))
#)
#ida.add_dependent_variable("ne", "rp", basis_functions,
#    initial_weights=6e19*np.linspace(1,0,len(basis_functions))**2,
#    #initial_weights=6e19*np.array([1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3, 0.1, .005, 0]),
#    initial_std_devs=np.ones(len(basis_functions))
#)

DataBaseIdentifier = DataBaseIdentifier()

equilibrium = ITER2DEquilibrium(
    db_experiment     = DataBaseIdentifier.equilibrium_experiment,
    db_pulse_number   = DataBaseIdentifier.equilibrium_pulse_number,
    db_edition        = DataBaseIdentifier.equilibrium_edition,
    db_input_user     = DataBaseIdentifier.equilibrium_input_user)

ece = SimpleElectronCyclotronEmission(
    rho_pol=rp,
    Te=Te,
    #likelihood=GaussianLikelihood())
    likelihood=CauchyLikelihood(0.5))

ts = SimpleThomsonScattering(
    rho_pol=rp,
    Te=Te,
    ne=ne,
    #likelihood=GaussianLikelihood())
    likelihood=CauchyLikelihood(0.5))

ify = SimpleInterferometry(
    db_experiment     = DataBaseIdentifier.interferometry_experiment,
    db_pulse_number   = DataBaseIdentifier.interferometry_pulse_number,
    db_edition        = DataBaseIdentifier.interferometry_edition,
    db_input_user     = DataBaseIdentifier.interferometry_input_user,
    input_param_file  = DataBaseIdentifier.input_param_file,
    rho_pol           = rp,
    ne                = ne,
    equilibrium       = equilibrium,
    likelihood        = GaussianLikelihood())


pp = Positivity1DPrior(states=[Te, ne])

ida.add_diagnostic(ece)
ida.add_diagnostic(ts)
ida.add_diagnostic(ify)
ida.add_prior(pp)

ida.prepare() # without time_interval here since no time dependency in sample

if 0:  # only optimise - no time measurement
    ida.optimise()
elif 0:  # test time once
    import time
    t0 = time.time()
    res = ida.optimize()
    t1 = time.time()
    print("Optimisation took {:.6f} s".format(t1-t0))
else:  # evaluate several times
    Ntest = 5
    timeVec = np.zeros(Ntest)
    import time
    for i in range(Ntest):
        t0 = time.time()
        res = ida.optimize()
        t1 = time.time()
        timeVec[i] = t1-t0

        if i < Ntest-1:  # reset parameters for next optimisation
            # an initial guess to shorten the optimisation:
            Te.set_parameters(1e3*np.linspace(1,0,len(knots))**2)
            ne.set_parameters(6e19*np.linspace(1,0,len(knots))**2)
            if model.__name__ in ['ExpCubicSplineDependent1DVariable', 'ExpUnivSplineDependent1DVariable']:
                # adapted scale when using something on the lines of exp(par)
                Te.set_parameters(np.log(1e3*np.linspace(1,0,len(knots))**2 + 1e0))
                ne.set_parameters(np.log(6e19*np.linspace(1,0,len(knots))**2 + 1e16))

    print("Optimisation took {:.6f} +- {:.6f} s".format(
        timeVec.mean(), timeVec.std()))
    print("ranging from {:.6f} to {:.6f} s".format(
        timeVec.min(), timeVec.max()))

# In[1]:
# plotting prep
rhos = np.linspace(0, 1.2, 100)
lines = 100
plotDx = True
plotDx2 = False
if plotDx:
    lines += 100
if plotDx2:
    lines += 100
plt.figure(1)
plt.clf()
ax1 = plt.subplot(lines + 21)
ax2 = plt.subplot(lines + 22)
if plotDx:
    ax12 = plt.subplot(lines + 23)
    ax22 = plt.subplot(lines + 24)
if plotDx2:
    ax13 = plt.subplot(lines + 25)
    ax23 = plt.subplot(lines + 26)
ax1.grid()
ax2.grid()

Te_scal = 1.0e-3
ne_scal = 1.0e-19

# plot fit result
Tes = ida.get_dependent_variable("Te").evaluate(rhos)
ax1.plot(rhos, Tes, alpha=1)

nes = ida.get_dependent_variable("ne").evaluate(rhos)
ax2.plot(rhos, nes, alpha=1)

if plotDx:  # and derivative
    TesDxInt = ida.get_dependent_variable("Te").evaluate(rhos, dn=1)
    TesDxFd = np.gradient(Tes, rhos)
    nesDxInt = ida.get_dependent_variable("ne").evaluate(rhos, dn=1)
    nesDxFd = np.gradient(nes, rhos)
    ax12.plot(rhos, TesDxInt, 'k-')
    ax12.plot(rhos, TesDxFd, 'r--')
    ax22.plot(rhos, nesDxInt, 'k-')
    ax22.plot(rhos, nesDxFd, 'r--')
if plotDx2:  # ... and second derivative
    TesDx2Int = ida.get_dependent_variable("Te").evaluate(rhos, dn=2)
    TesDx2Fd = np.gradient(np.gradient(Tes, rhos), rhos)
    ax13.plot(rhos, TesDx2Int, 'k-')
    ax13.plot(rhos, TesDx2Fd, 'r--')
    nesDx2Int = ida.get_dependent_variable("ne").evaluate(rhos, dn=2)
    nesDx2Fd = np.gradient(np.gradient(nes, rhos), rhos)
    ax23.plot(rhos, nesDx2Int, 'k-')
    ax23.plot(rhos, nesDx2Fd, 'r--')

#Te = ida.get_dependent_variable("Te").evaluate(rhos)
#Te_m, Te_s = unp.nominal_values(Te), unp.std_devs(Te)
#ax1.errorbar(ida.get_independent_variable("rp").values, Te_m, 0*Te_s, alpha=1)
#with open('ida_iter_te_fit.rp', 'w') as ff:
#    for i in range(len(rpp)):
#        print(rpp[i], Te_m[i]*Te_scal, file=ff)

#ne = ida.get_dependent_variable("ne").evaluate()
#ne_m, ne_s = unp.nominal_values(ne), unp.std_devs(ne)
#ax2.errorbar(ida.get_independent_variable("rp").values, ne_m, 0*ne_s, alpha=1)
#with open('ida_iter_ne_fit.rp', 'w') as ff:
#    for i in range(len(rpp)):
#        print(rpp[i], ne_m[i]*ne_scal, file=ff)

# plot sample TS
tsr, tsd = ts.get_data(apply_ne_norm=False)
tste, tsne = tsd[0]
tsr = tsr[0]
ax1.errorbar(tsr, unp.nominal_values(tste), unp.std_devs(tste))
ax2.errorbar(tsr, unp.nominal_values(tsne), unp.std_devs(tsne))
#with open('ida_iter_ts_te_dat.rp', 'w') as ff:
#    for i in range(len(tsr)):
#        print(tsr[i], unp.nominal_values(tste)[i]*Te_scal, unp.std_devs(tste)[i]*Te_scal, file=ff)

#with open('ida_iter_ts_ne_dat.rp', 'w') as ff:
#    for i in range(len(tsr)):
#        print(tsr[i], unp.nominal_values(tsne)[i]*ne_scal, unp.std_devs(tsne)[i]*ne_scal, file=ff)

# plot sample ECE
rps, data = ece.get_data()
for rp, d in zip(rps, data):
    Te_m_, Te_s_ = unp.nominal_values(d), unp.std_devs(d)
    ax1.errorbar(rp, Te_m_, Te_s_, ls='--', alpha=0.9)
    break

#ecer = rps[0]
#Te_m, Te_s = unp.nominal_values(data[0]), unp.std_devs(data[0])
#with open('ida_iter_ece_dat.rp', 'w') as ff:
#    for i in range(len(ecer)):
#        print(ecer[i], Te_m[i]*Te_scal, Te_s[i]*Te_scal, file=ff)


# plot sample Interferometry
ifyr, ifyd = ify.get_data(apply_ne_norm=False)
ifyne = ifyd[0]
ifyr = ifyr[0]
l_R = [14.967941620625727, 13.872168467835875, 11.856109160042251,
        9.424350890500168, 5.362957544917618]
#ax2.errorbar(ifyr, unp.nominal_values(ifyne), unp.std_devs(ifyne), ls=':', alpha=0.8)
ax2.errorbar(ifyr, unp.nominal_values(ifyne)/l_R, unp.std_devs(ifyne)/l_R, ls=':', alpha=0.8)

ify_m, ify_s = unp.nominal_values(ifyne), unp.std_devs(ifyne)
ify_m /= l_R
ify_s /= l_R
ify_fm = ify.forward_model(ifyr)
ify_fm = np.array(ify_fm[0]) * 1.e19 / l_R

#with open('ida_iter_ify_dat.rp', 'w') as ff:
#    for i in range(len(ifyr)):
#        print(ifyr[i], ify_m[i]*ne_scal, ify_s[i]*ne_scal, file=ff)

#with open('ida_iter_ify_fm.rp', 'w') as ff:
#    for i in range(len(ifyr)):
#        print(ifyr[i], ify_fm[i]*ne_scal, file=ff)

plt.show()


sys.exit()
