#!/usr/bin/env python

import sys
import os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))

import numpy as np
from uncertainties import unumpy as unp
import matplotlib.pyplot as plt
try:
    plt.switch_backend('TkAgg')
except Exception as Error:
    print("Could not set backend: {}".format(Error))
#; plt.ion()
from scipy.interpolate import RegularGridInterpolator, CubicSpline, interp1d

from ida.core.ida import IDA
from ida.core.variables import Independent1DVariable, CubicSplineDependent1DVariable, UnivSplineDependent1DVariable, \
    ExpUnivSplineDependent1DVariable
from ida.core.likelihoods import GaussianLikelihood, CauchyLikelihood
from ida.core.priors import Positivity1DPrior, Monotonicity1DPrior
from ida.diagnostics.sample.ECE import SimpleElectronCyclotronEmission
from ida.diagnostics.sample.TS import SimpleThomsonScattering
from ida.diagnostics.iter.TIP import SimpleInterferometry
from ida.diagnostics.iter.iter_equilibrium import ITER2DEquilibrium

###############################################################
# ADDITIONAL CLASS FOR DATABASE IDENTIFIER
###############################################################
class DataBaseIdentifier:
    def __init__(self):
        from datetime import datetime
        import os

        #self.equilibrium_experiment      = 'ITER'    # for ITER lokin nodes
        self.equilibrium_experiment      = 'iterdb'  # for tok0X nodes
        self.equilibrium_pulse_number    = 100003
        self.equilibrium_edition         = 1
        self.equilibrium_input_user      = 'public'

        #self.interferometry_experiment   = 'ITER'
        self.interferometry_experiment   = self.equilibrium_experiment
        self.interferometry_pulse_number = self.equilibrium_pulse_number
        self.interferometry_edition      = 1
        self.interferometry_input_user   = 'public'

        self.output_experiment           = 'iter'
        self.user                        = os.getenv("USER")                # user running the code

        self.imas_version                = os.getenv('IMAS_VERSION')[0]     # IMAS DATA VERSION

        # CURRENT DATE
        now                   = datetime.now()
        self.today            = now.strftime("%y-%m-%d")
        self.input_param_file = "iter_parameters.xml"


#rp = np.linspace(0,1.2, 100)
rp = Independent1DVariable('rho_pol', (0, 1.2))

#knots = np.array([0, 0.5, 0.9, 0.95, 1, 1.05, 1.2])
knots = np.array([0, 0.5, 0.8, 0.9, 1, 1.10, 1.2])
#knots = np.array([0, 0.2, 0.4, 0.6, 0.8, 0.9, 0.95, 1, 1.09, 1.2])
#knots = np.array([0, 1, 1.2])

#basis_functions = []
#for cv in np.eye(len(knots)):
#    func = CubicSpline(knots, cv, bc_type=('clamped','clamped'), extrapolate=True)
#    # func = interp1d(rp, np.exp(CubicSpline(knots, cv, bc_type=('clamped','clamped'), extrapolate=True)(rp)),
#    #        bounds_error=False, fill_value="extrapolate")
#    basis_functions.append(func)

if 0:  # old cubic spline
    Te = CubicSplineDependent1DVariable('Te', rp, knots, unit="eV", scale=1e3)
    ne = CubicSplineDependent1DVariable('ne', rp, knots, unit="m**-3", scale=1e19)
elif 0:  # new cubic spline
    Te = UnivSplineDependent1DVariable('Te', rp, knots, unit="eV", scale=1e3)
    ne = UnivSplineDependent1DVariable('ne', rp, knots, unit="m**-3", scale=1e19)
else:  # exp of new cubic spline
    Te = ExpUnivSplineDependent1DVariable('Te', rp, knots, unit="eV", scale=1e3)
    ne = ExpUnivSplineDependent1DVariable('ne', rp, knots, unit="m**-3", scale=1e19)

# an initial guess to shorten the optimisation:
Te_p0 = Te.suggest_parameters(1e3*np.linspace(1,0,len(knots))**2)
ne_p0 = ne.suggest_parameters(6e19*np.linspace(1,0,len(knots))**2)

Te.set_parameters(Te_p0)
ne.set_parameters(ne_p0)

ida = IDA()
ida.add_independent_variable(rp)
ida.add_dependent_variable(Te)
ida.add_dependent_variable(ne)
#ida.add_independent_variable("rp", rp)
#ida.add_dependent_variable("Te", "rp", basis_functions,
#    initial_weights=3e3*np.linspace(1,0,len(basis_functions))**2,
#    #initial_weights=3e3*np.array([1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3, 0.1, .005, 0]),
#    initial_std_devs=1e2*np.ones(len(basis_functions))
#)
#ida.add_dependent_variable("ne", "rp", basis_functions,
#    initial_weights=6e19*np.linspace(1,0,len(basis_functions))**2,
#    #initial_weights=6e19*np.array([1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3, 0.1, .005, 0]),
#    initial_std_devs=np.ones(len(basis_functions))
#)

DataBaseIdentifier = DataBaseIdentifier()

equilibrium = ITER2DEquilibrium(
    db_experiment     = DataBaseIdentifier.equilibrium_experiment,
    db_pulse_number   = DataBaseIdentifier.equilibrium_pulse_number,
    db_edition        = DataBaseIdentifier.equilibrium_edition,
    db_input_user     = DataBaseIdentifier.equilibrium_input_user)

ece = SimpleElectronCyclotronEmission(
    rho_pol=rp,
    Te=Te,
    #likelihood=GaussianLikelihood())
    likelihood=CauchyLikelihood(0.5))

ts = SimpleThomsonScattering(
    rho_pol=rp,
    Te=Te,
    ne=ne,
    #likelihood=GaussianLikelihood())
    likelihood=CauchyLikelihood(0.5))

ify = SimpleInterferometry(
    db_experiment     = DataBaseIdentifier.interferometry_experiment,
    db_pulse_number   = DataBaseIdentifier.interferometry_pulse_number,
    db_edition        = DataBaseIdentifier.interferometry_edition,
    db_input_user     = DataBaseIdentifier.interferometry_input_user,
    input_param_file  = DataBaseIdentifier.input_param_file,
    rho_pol           = rp,
    ne                = ne,
    equilibrium       = equilibrium,
    likelihood        = GaussianLikelihood())

# TODO: define priors relevant to exp(parameter) and maybe make suggestion from model used.
# pp = Positivity1DPrior(states=[Te, ne])  # not appropriate for exp(spline)

ida.add_diagnostic(ece)
ida.add_diagnostic(ts)
ida.add_diagnostic(ify)
# ida.add_prior(pp)

ida.prepare() # without time_interval here since no time dependency in sample

res = ida.optimize()


# In[1]:

# Set matplotlib parameters
try:
    plt.rc('text', usetex=True) # make sure to use raw string like r'$x^2$'
    plt.rc('figure', figsize=[10, 6])
    plt.rc('lines', linewidth=2.0)
    plt.rc('lines', markersize=3)
    plt.rcParams['axes.grid'] = True # always plot grid
    plt.rc('font', size=16)
except Exception as Error:
    print("Could not set all plt.rc values: {}".format(Error))
    1/0

# plotting prep
rhos = np.linspace(0, 1.2, 100)

fig, ax = plt.subplots(len(ida.dependent_variables), 1, sharex=True)

ax1 = ax[0]
ax2 = ax[1]
ax1.grid()
ax2.grid()

ax1.set_title('{} $\sharp${}'.format(DataBaseIdentifier.equilibrium_experiment, DataBaseIdentifier.equilibrium_pulse_number))
# plot fit result
# Tes = ida.get_dependent_variable("Te").evaluate(rhos)
# ax1.plot(rhos, Tes * Te_scal, alpha=1, label='IDA')

# nes = ida.get_dependent_variable("ne").evaluate(rhos)
# ax2.plot(rhos, nes * ne_scal, alpha=1, label='IDA')
for idx, name in enumerate(ida.get_dependent_variable_name()):  # access to member variable is not desired
    dv = ida.get_dependent_variable(name)
    aa = ax[idx]
    aa.set_title('dependent variable {}'.format(dv.name))
    aa.plot(rhos,
            dv.evaluate(rhos) / dv.scale,
            label='IDA')
    aa.set_ylabel('{} / {}'.format(dv.name, dv.unit))
# ax2.set_ylabel(r'$n_e$ / $10^{19} / \mathrm{m}^3$')

# for plotting of simplified data scale is manually set
Te_scal = 1.e3
ne_scal = 1.e19

#Te = ida.get_dependent_variable("Te").evaluate(rhos)
#Te_m, Te_s = unp.nominal_values(Te), unp.std_devs(Te)
#ax1.errorbar(ida.get_independent_variable("rp").values, Te_m, 0*Te_s, alpha=1)
#with open('ida_iter_te_fit.rp', 'w') as ff:
#    for i in range(len(rpp)):
#        print(rpp[i], Te_m[i]*Te_scal, file=ff)

#ne = ida.get_dependent_variable("ne").evaluate()
#ne_m, ne_s = unp.nominal_values(ne), unp.std_devs(ne)
#ax2.errorbar(ida.get_independent_variable("rp").values, ne_m, 0*ne_s, alpha=1)
#with open('ida_iter_ne_fit.rp', 'w') as ff:
#    for i in range(len(rpp)):
#        print(rpp[i], ne_m[i]*ne_scal, file=ff)

# plot sample TS
tsr, tsd = ts.get_data(apply_ne_norm=False)
tste, tsne = tsd[0]
tsr = tsr[0]
ax1.errorbar(tsr, unp.nominal_values(tste) / Te_scal, unp.std_devs(tste) / Te_scal, label='TS Data',
             ls='', lw=3,
             marker='_', ms=10, mew=2)
ax2.errorbar(tsr, unp.nominal_values(tsne) / ne_scal, unp.std_devs(tsne) / ne_scal, label='TS Data',
             ls='',
             marker='_', ms=10, mew=2)
ax2.set_xlabel(r'$\rho_\mathrm{pol}$')
#with open('ida_iter_ts_te_dat.rp', 'w') as ff:
#    for i in range(len(tsr)):
#        print(tsr[i], unp.nominal_values(tste)[i]/Te_scal, unp.std_devs(tste)[i]/Te_scal, file=ff)

#with open('ida_iter_ts_ne_dat.rp', 'w') as ff:
#    for i in range(len(tsr)):
#        print(tsr[i], unp.nominal_values(tsne)[i]/ne_scal, unp.std_devs(tsne)[i]/ne_scal, file=ff)

# plot sample ECE
rps, data = ece.get_data()
for rp, d in zip(rps, data):
    Te_m_, Te_s_ = unp.nominal_values(d), unp.std_devs(d)
    ax1.errorbar(rp,
                 Te_m_ / Te_scal,
                 Te_s_ / Te_scal, ls='',
                 marker='_', ms=10, mew=2,
                 alpha=0.9, label='ECE data')
    break  # plot only first sample

#ecer = rps[0]
#Te_m, Te_s = unp.nominal_values(data[0]), unp.std_devs(data[0])
#with open('ida_iter_ece_dat.rp', 'w') as ff:
#    for i in range(len(ecer)):
#        print(ecer[i], Te_m[i]/Te_scal, Te_s[i]/Te_scal, file=ff)


# plot sample Interferometry
ifyr, ifyd = ify.get_data(apply_ne_norm=False)
ifyne = ifyd[0]
ifyr = ifyr[0]
l_R = [14.967941620625727, 13.872168467835875, 11.856109160042251,
        9.424350890500168, 5.362957544917618]
#ax2.errorbar(ifyr, unp.nominal_values(ifyne), unp.std_devs(ifyne), ls=':', alpha=0.8)
ax2.errorbar(ifyr,
             unp.nominal_values(ifyne) / l_R  / ne_scal,
             unp.std_devs(ifyne) / l_R / ne_scal,
             ls='',
             marker='_', ms=15, mew=2,
             alpha=0.8, label='TIF data')

ify_m, ify_s = unp.nominal_values(ifyne), unp.std_devs(ifyne)
ify_m /= l_R
ify_s /= l_R
ify_fm = ify.forward_model(ifyr)
ify_fm = np.array(ify_fm[0]) * 1.e19 / l_R

#with open('ida_iter_ify_dat.rp', 'w') as ff:
#    for i in range(len(ifyr)):
#        print(ifyr[i], ify_m[i]*ne_scal, ify_s[i]*ne_scal, file=ff)

#with open('ida_iter_ify_fm.rp', 'w') as ff:
#    for i in range(len(ifyr)):
#        print(ifyr[i], ify_fm[i]*ne_scal, file=ff)


# mimic the array of plt.subplots, which is handy for this stuff
ax = [ax1, ax2]
for aa in ax:
    aa.legend(loc=0)
    aa.set_xlim([max(0, aa.get_xlim()[0]), aa.get_xlim()[1]])  # limit lower x_lim to zero

plt.show()


sys.exit()
