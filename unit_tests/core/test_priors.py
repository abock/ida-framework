import unittest
import numpy as np
from ida.core.priors import *
from ida.core.dependent_variables import DependentBSplineProfile
from ida.core.independent_variables import IndependentProfile1DVariable

def create_state():
    rho, Te = np.loadtxt("unit_tests/core/Te.dat", 
                         skiprows=1, unpack=True)
    axis = np.linspace(0,1.2,200)
    independents = [IndependentProfile1DVariable("rho_pol", axis)]
    state = [DependentBSplineProfile(independent_variables=independents)]
    state[0].set_default_bounds()
    state[0].make_initital_guess(Te[rho < 1.2] * 1.e3 / 2.0, 
                                    None, rho[rho < 1.2])
    return state

class TestPositivityPrior(unittest.TestCase):

    def test_prior(self):
        state = create_state()
        prior = PositivityPrior(state)
        self.assertAlmostEqual(prior.get_likelihood(), 0)

class TestSOLTePrior(unittest.TestCase):

    def test_prior(self):
        state = create_state()
        prior = SOLTePrior(state, 1.0, 100.e0)
        self.assertAlmostEqual(prior.get_likelihood(), 0)

class TestMonoticityPrior(unittest.TestCase):

    def test_prior(self):
        state = create_state()
        prior = MonoticityPrior(state, 50.0, 1.e17, 1.1, 1.3)
        self.assertAlmostEqual(prior.get_likelihood(), 1335.1768777756622)
        print(prior.get_likelihood())

class TestCurvaturePrior(unittest.TestCase):

    def test_prior(self):
        state = create_state()
        prior = CurvaturePrior(state, 5.e2, 0.98, 0.02, 0.1, 1.005, 0.03, 0.1)
        self.assertAlmostEqual(prior.get_likelihood(), 3.670651993143269)


if __name__ == '__main__':
    unittest.main()