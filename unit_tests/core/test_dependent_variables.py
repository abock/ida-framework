import unittest
import numpy as np
from ida.core.dependent_variables import *
from ida.core.independent_variables import *

class TestDependentBSplineProfile(unittest.TestCase):

    def test_initial_guess(self):
        rho, Te = np.loadtxt("unit_tests/core/Te.dat", 
                             skiprows=1, unpack=True)
        axis = np.linspace(0,1.2,200)
        independents = [IndependentProfile1DVariable("rho_pol", axis)]
        spl = DependentBSplineProfile(independent_variables=independents)
        spl.set_default_bounds()
        spl.make_initital_guess(Te[rho < 1.2] * 1.e3 / 2.0, 
                                None, rho[rho < 1.2])
        test_parameters = np.array([ 7.36925929,  7.26948691,
                7.20782926, 6.88754744, 6.50697529, 6.26600331,
                6.15968872, 5.94907576, 5.72418639, 5.4786442,
                5.36901852, 5.08800608, 4.42544428, 3.40832514,
                3.0371525, 2.40747499, 1.18550843, -1.90743411,
                -3.38257687, -3.36656255, -2.65857946])
        for param, test_param in zip(spl.parameters, test_parameters):
            self.assertAlmostEqual(param, test_param)
        self.assertAlmostEqual(spl.eval(0.6), 630.3591038777062)
        rho, ne = np.loadtxt("unit_tests/core/ne.dat", 
                             skiprows=1, unpack=True)
        spl = DependentBSplineProfile(independent_variables=independents)
        spl.set_default_bounds()
        spl.make_initital_guess(ne[rho < 1.2] * 1.e19 / 2.0, 
                                None, rho[rho < 1.2])
        test_parameters = np.array([45.03624, 45.02218115, 44.97031171,
                                    44.90707032, 44.84255934, 44.79211885,
                                    44.77055481, 44.78117435, 44.77666296,
                                    44.79641488, 44.72074663, 44.49487006,
                                    44.19888854, 43.56609252, 43.01152042,
                                    42.41536124, 42.35215852, 40.54340469,
                                    37.82697925, 37.52264592, 37.2709213])
        for param, test_param in zip(spl.parameters, test_parameters):
            self.assertAlmostEqual(param, test_param)
        self.assertAlmostEqual(spl.eval(0.6), 2.9439056947069653e+19)

class TestGenericBasisFunction(unittest.TestCase):

    def test_eval(self):
        from scipy.interpolate import CubicSpline
        rp = np.linspace(0,1.2, 100)
        knots = np.array([0, 0.5, 0.9, 0.95, 1, 1.05, 1.2])
        #knots = np.array([0, 1, 1.2])
        basis_functions = []
        for cv in np.eye(len(knots)):
            func = CubicSpline(knots, cv, bc_type=('clamped','clamped'), extrapolate=True)
            # func = interp1d(rp, np.exp(CubicSpline(knots, cv, bc_type=('clamped','clamped'), extrapolate=True)(rp)), 
            #        bounds_error=False, fill_value="extrapolate")
            basis_functions.append(func)
        spl = GenericBasisFunction("Cubic spline", basis_functions, 
                parameters=1.e3*np.linspace(1,0,len(basis_functions))**2, 
                independent_variables=[IndependentProfile1DVariable("rho_pol", rp)], 
                out_of_bounds_value=np.nan)
        spl.set_default_bounds()
        self.assertAlmostEqual(spl.eval(0.6), 731.3187149764193)

if __name__ == '__main__':
    unittest.main()