import unittest
import numpy as np
from ida.core.likelihoods import *

def get_test_data():
    data = np.loadtxt("unit_tests/core/test_data.dat")
    uncertainties = np.loadtxt("unit_tests/core/test_uncertainties.dat")
    models = np.loadtxt("unit_tests/core/test_synth_data.dat")
    return data, uncertainties, models

class TestGaussianLikelihood(unittest.TestCase):

    def test_likelihood(self):
        data, uncertainties, models = get_test_data()
        likelihood = GaussianLikelihood()
        self.assertAlmostEqual(5715.352151138136, 
              likelihood.eval(data, uncertainties, models))

class TestCauchyLikelihood(unittest.TestCase):

    def test_likelihood(self):
        data, uncertainties, models = get_test_data()
        likelihood = CauchyLikelihood(0.5)
        self.assertAlmostEqual(295.7609917756257, 
              likelihood.eval(data, uncertainties, models))

if __name__ == '__main__':
    unittest.main()