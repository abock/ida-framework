#!/usr/bin/env python

class BaseDiagnostic(object):
    """
    BaseDiagnostic holds the fundamental interfaces of all diagnostic objects.
    All diagnostic objects should be derived from BaseDiagnostic.
    """
    def __init__(self):
        super(BaseDiagnostic, self).__init__()
        self.t0, self.t1 = None, None

    
    def prepare(self, time_interval=None, **kwargs):
        """
        This method is used to bring the diagnostic into a certain state, e.g. prepare data for a certain time.
        """
        if time_interval:
            self.t0, self.t1 = time_interval

    def get_data(self):
        """
        This method is used to yield data. Use prepare() to prepare the data.
        """
        return np.nan

    def forward_model(self):
        """
        This method is used to yield the result of a forward model. Use prepare() to prepare the model.
        """
        return np.nan

    def get_likelihood(self):
        """
        Yields likelihood of model with respect to data.
        Use prepare() to bring the diagnostic object in the correct state beforehand.
        """
        return np.nan
