#!/usr/bin/env python

import numpy as np
from scipy.interpolate import RegularGridInterpolator


class BaseEquilibrium(object):
    """docstring for BaseEquilibrium."""
    def __init__(self):
        super(BaseEquilibrium, self).__init__()


class Base2DEquilibrium(BaseEquilibrium):
    """docstring for Base2DEquilibrium."""
    def __init__(self, tvec = None, Rvec = None, zvec = None, 
        pft = None, psi_ax = None, psi_lcfs = None):
        super(Base2DEquilibrium, self).__init__()

        # time/space vectors [seconds, metres]
        self.tvec = tvec # time vector [seconds]
        self.Rvec = Rvec # R vector [metres]
        self.zvec = zvec # z vector [metres]
        self.pft =  pft # poloidal flux tensor
        self.psi_ax = psi_ax # flux(time) on axis   
        self.psi_lcfs = psi_lcfs # flux(time) at last closed flux surface (limiter or x-point)
        
        #self.rpt = np.sqrt((self.pft.T - self.psi_ax)/(self.psi_lcfs - self.psi_ax)).T
        self.rpt = np.sqrt((np.transpose(self.pft) - self.psi_ax)/(self.psi_lcfs - self.psi_ax)).T  #rrf
        self.pf_rgi = RegularGridInterpolator((tvec, zvec, Rvec), self.pft,
                                  bounds_error=False, fill_value=None)
        self.rp_rgi = RegularGridInterpolator((self.tvec, self.zvec, self.Rvec), self.rpt,
                                  #bounds_error=False, fill_value=np.nan)
                                  bounds_error=False, fill_value=None) #rrf: if None, values outside the domain are extrapolated.

    def cartesian2magnetic(self, t_in, Rz_in, coord_out = "rho_pol"):
        """
        Map from t,R,z to magnetic coordinates.
        """
        interpolators = {
            'rho_pol': self.rp_rgi,
            'psi': self.pf_rgi,
            'psi_N': lambda pts: self.rp_rgi(pts)**2,
        }
        if coord_out not in interpolators:
            raise NotImplementedError
        t_in = np.atleast_1d(t_in)
        Rz_in = np.atleast_2d(Rz_in)
        Rz_in[...,[0,1]] = Rz_in[...,[1,0]]
        intp_in = np.empty((len(t_in)*len(Rz_in), 3))
        intp_in[:,0] = np.repeat(t_in, len(Rz_in))
        intp_in[:, 1:] = np.tile(Rz_in.T, len(t_in)).T
        return interpolators[coord_out](intp_in).reshape(len(t_in), len(Rz_in))

