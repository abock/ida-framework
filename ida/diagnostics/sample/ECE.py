#!/usr/bin/env python

import numpy as np
import sys
import os
from uncertainties import unumpy as unp
from scipy.interpolate import interp1d
from ..base_diagnostic import BaseDiagnostic
# this is not the cleanest but the easiest way to do reliable sibling imports:
_ = os.path.join(os.path.dirname(__file__), '..')
sys.path.insert(0, _) 
from ida.forward_models.sample.sample_ece import SampleECEForwardModel
sys.path.remove(_)
from IPython import embed

class SimpleElectronCyclotronEmission(BaseDiagnostic):
    """Simple ECE diagnostic sample."""
    def __init__(self, rho_pol, Te, likelihood):
        super(BaseDiagnostic, self).__init__()
        self.rp_coord = rho_pol
        self.likelihood = likelihood
        self.rps = np.linspace(rho_pol.bounds[0], rho_pol.bounds[1], 1000)
        self._forward_model = SampleECEForwardModel(Te, self.rps)
 
    def prepare(self, time_interval, **kwargs):
        """
        Yields experimental data.
        """

        #te = interp1d(self.rp_coord.values, unp.nominal_values(self.Te_state.evaluate()),
        #                         bounds_error=False,fill_value="extrapolate")(rp)
        #with open('te.rp', 'w') as f1:
        #    for i in range(100):
        #        print(rp[i],te[i],file=f1)

        # end test data

        np.random.seed(0) # data should be noisy, but static
        x = np.array([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99, 1.0, 1.01, 1.05, 1.1])

        #y = interp1d(self.rp_coord.values, unp.nominal_values(self.Te_state.evaluate()),
        #             bounds_error=False,fill_value="extrapolate")(x)
        #print("ECE data", y)
        #quit("EEE")
        y = np.array([3000.,         2897.87064207, 2700.11756238, 2535.05049831, 2400.,
                      2261.9353853,  2099.92550604, 1917.20397758, 1800.,         1497.56562548,
                      900.05373484,  399.76238684,  303.35656372,  223.13301348,
                      51.1315693, 12.47085195])

        #y = np.array([3000.        , 2884.53596659, 2629.52483076, 2370.5476609 ,
        #              2208.84161744, 2109.14735679, 2001.0517425 , 1814.61321681,
        #              1530.76797623, 1333.61072439, 1221.79886705,  760.0983799 ,
        #               597.38637871,  452.51709159,  149.73507464,   37.39527476]) 
        #y *= (1 + np.random.rand(len(y))*0.1)
        #y = unp.uarray(y, np.clip(np.abs(y)*0.1, 50, None))
        y = np.random.normal(y,np.clip(np.abs(y)*0.1, 5., None),len(y))
        y = unp.uarray(y, np.clip(np.abs(y)*0.1, 5, None))

        self.data_to_return = x, y # data prepared for get_data()
        self._forward_model.prepare(time_interval) # just for illustration
        return super().prepare(time_interval, **kwargs) 


    def get_data(self):
        """
        doc
        """
        x, y = self.data_to_return
        return [x], [y]

    def forward_model(self, rho_pols):
        """
        Computes forward model. SimpleECE: Trad = Te
        """
        return self._forward_model.eval(rho_pols, bounds_error=False, fill_value="extrapolate")
        #return interp1d(self.rp_coord.values, unp.nominal_values(self.Te_state.evaluate()),
        #    bounds_error=False, fill_value="extrapolate")(rho_pols)

    def get_likelihood(self):
        """
        Yields likelihood of model with respect to data.
        """
        r, d = self.get_data()
        fm = self.forward_model(r)
        likeli = self.likelihood.eval(data=unp.nominal_values(d), uncertainty=unp.std_devs(d), model=fm)
        #print("ECE", likeli)
        return likeli

