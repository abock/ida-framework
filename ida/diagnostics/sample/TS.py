#!/usr/bin/env python

import numpy as np
import os
from uncertainties import unumpy as unp
from scipy.interpolate import interp1d
from ..base_diagnostic import BaseDiagnostic
from copy import copy

class SimpleThomsonScattering(BaseDiagnostic):
    """Simple Thomson Scattering sample diagnostic."""
    def __init__(self, rho_pol, Te, ne, likelihood, ne_state_norm = 1e19, ne_data_norm=1e19):
        super(BaseDiagnostic, self).__init__()
        self.Te_state = Te
        self.ne_state = ne
        self.rp_coord = rho_pol
        self.likelihood = likelihood
        self.ne_state_norm = ne_state_norm
        self.ne_data_norm = ne_data_norm
        self.rps = np.linspace(rho_pol.bounds[0], rho_pol.bounds[1], 1000)
    
    def prepare(self, time_interval, **kwargs):
        """
        Yields experimental data.
        """      

        #test data
        #rp = np.linspace(0,1.2,100)
        #te = interp1d(self.rp_coord.values, unp.nominal_values(self.Te_state.evaluate()),
        #                         bounds_error=False,fill_value="extrapolate")(rp)
        #ne = interp1d(self.rp_coord.values, unp.nominal_values(self.ne_state.evaluate()),
        #                         bounds_error=False,fill_value="extrapolate")(rp)
        #with open('te_org.rp', 'w') as f1:
        #    for i in range(100):
        #        print(rp[i],te[i],file=f1)

        #with open('ne_org.rp', 'w') as f1:
        #    for i in range(100):
        #        print(rp[i],ne[i],file=f1)

        #quit("www")
        # end test data

        #return  
        np.random.seed(22) # data should be noisy, but static
        x = np.linspace(0.05, 0.95, 19)

        #yTe = interp1d(self.rp_coord.values, unp.nominal_values(self.Te_state.evaluate()),
        #                         bounds_error=False,fill_value="extrapolate")(x)
        #yne = interp1d(self.rp_coord.values, unp.nominal_values(self.ne_state.evaluate()),
        #                         bounds_error=False,fill_value="extrapolate")(x)
        #print("TS Te", yTe)
        #print("TS ne", yne)
        #quit("TSTS")
        yTeo = np.array([2970.98611099, 2897.87064207, 2800.85016117, 2700.11756238, 2611.58757131,
                        2535.05049831, 2466.0184852,  2400.,         2332.84592172, 2261.9353853,
                        2185.03795075, 2099.92550604, 2007.08082093, 1917.20397758, 1843.70593997,
                        1800.,         1754.47193161, 1497.56562548,  900.05373484])
        yneo = np.array([5.94197222e+19, 5.79574128e+19, 5.60170032e+19, 5.40023512e+19,
                        5.22317514e+19, 5.07010100e+19, 4.93203697e+19, 4.80000000e+19,
                        4.66569184e+19, 4.52387077e+19, 4.37007590e+19, 4.19985101e+19,
                        4.01416164e+19, 3.83440796e+19, 3.68741188e+19, 3.60000000e+19,
                        3.50894386e+19, 2.99513125e+19, 1.80010747e+19])

        #yTeo = np.array([3067.14180987, 2930.59436911, 2774.54020063, 2619.16969586,
        #                2484.67238574, 2385.99392147, 2317.83241726, 2269.61524014,
        #                2230.47085458, 2189.74937471, 2136.80091465, 2060.97558851,
        #                1951.62470622, 1805.38682723, 1647.03785603])
        #yneo = np.array([4.80755994e+19, 4.60999987e+19, 4.38254138e+19, 4.15343068e+19,
        #                3.95091277e+19, 3.79595224e+19, 3.68140270e+19, 3.59278874e+19,
        #                3.51524559e+19, 3.43420189e+19, 3.33508624e+19, 3.20332727e+19,
        #                3.02435536e+19, 2.79438628e+19, 2.55127996e+19])

        yTe = np.random.normal(yTeo,np.clip(np.abs(yTeo)*0.1, 50., None),len(yTeo))
        yne = np.random.normal(yneo,np.clip(np.abs(yneo)*0.1, 0.5, None),len(yneo))

        #with open('ida_iter_ts_te_dat_check.rp', 'w') as ff:
        #    for i in range(len(yTe)):
        #        print(x[i], yTeo[i], yTe[i], file=ff)

        #with open('ida_iter_ts_ne_dat_check.rp', 'w') as ff:
        #    for i in range(len(yne)):
        #        print(x[i], yneo[i], yne[i], file=ff)

        #quit("www")

        yTe = unp.uarray(yTe, np.clip(np.abs(yTeo)*0.1, 50, None))
        yne = unp.uarray(yne, np.clip(np.abs(yneo)*0.1, 0.5, None))

        self.data_to_return = x, yTe, yne # data prepared for get_data()
        return super().prepare(time_interval, **kwargs) 


    def get_data(self, apply_ne_norm = True):
        x, yTe, yne = self.data_to_return
        if apply_ne_norm: yne = copy(yne) / self.ne_data_norm
        return [x], [np.array((yTe, yne))]

    def forward_model(self, rho_pols):
        """
        Computes forward model.
        """
        return np.array((
            interp1d(self.rps, unp.nominal_values(self.Te_state.evaluate(self.rps)),
                bounds_error=False, fill_value="extrapolate")(rho_pols),
            interp1d(self.rps, unp.nominal_values(self.ne_state.evaluate(self.rps)),
                bounds_error=False, fill_value="extrapolate")(rho_pols)/self.ne_state_norm,
        ))

    def get_likelihood(self):
        """
        Yields likelihood of model with respect to data.
        """
        r, d = self.get_data()
        d = np.array(d).ravel()
        fm = self.forward_model(r).ravel()
        likeli = self.likelihood.eval(data=unp.nominal_values(d), uncertainty=unp.std_devs(d), model=fm)
        #print("TS", likeli)
        return likeli

