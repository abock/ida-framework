#!/usr/bin/env python

import numpy as np
import os
from uncertainties import unumpy as unp
from scipy.interpolate import interp1d
from ..aug import local_dd as dd
from ..base_diagnostic import BaseDiagnostic
from IPython import embed

class SimpleAUGElectronCyclotronEmission(BaseDiagnostic):
    """docstring for SimpleElectronCyclotronEmission."""
    def __init__(self, rho_pol, Te, equilibrium, likelihood,
                 dd_pulse_number,
                 dd_experiment="AUGD", dd_edition=0,
                 cache=None):
        super(BaseDiagnostic, self).__init__()
        
        self.tvec, self.Rs, self.zs, self.Trad = self.load_data(
            dd_pulse_number, dd_experiment, dd_edition, cache)

        self.equilibrium = equilibrium
        self.Te_state = Te
        self.rp_coord = rho_pol
        self.likelihood = likelihood

    def load_data(self, dd_pulse_number, dd_experiment, dd_edition, cache, dd_diag='CEC'):
        cachefile = cache+'/augece_%s_%s_%i_%i.npz'%(dd_experiment, dd_diag,
            dd_pulse_number, dd_edition)
        if cache is None:
            return self.load_data_dd(dd_pulse_number, dd_diag, dd_experiment, dd_edition)
        else:
            if os.path.isfile(cachefile):
                print('Found cached data in %s, loading now...'%cachefile)
                data = np.load(cachefile, allow_pickle=True)
                return data['tvec'], data['Rs'], data['zs'], data['Trad']
            else:
                tvec, Rs, zs, Trad = self.load_data_dd(dd_pulse_number, dd_diag, dd_experiment, 
                    dd_edition)
                np.savez(cachefile, tvec=tvec, Rs=Rs, zs=zs, Trad=Trad)
                return tvec, Rs, zs, Trad
    
    def load_data_dd(self, dd_pulse_number, dd_diag, dd_experiment, dd_edition):
        sf = dd.shotfile(dd_diag, dd_pulse_number, dd_experiment, dd_edition)

        Rs = sf.getObjectData('R-A')
        zs = sf.getObjectData('z-A')
        Rz_t = sf.getObjectData('rztime')[:len(Rs.T)]
        enabled_mask = (np.sum(Rs,axis=1) != 0)
        trad = sf.getObjectData('Trad-A')[enabled_mask].T
        tvec = sf.getObjectData('time-A')[:len(trad)]
        Trad = unp.uarray(trad, np.clip(np.abs(trad)*0.05, 50, None))
        Rs = interp1d(Rz_t, Rs[enabled_mask], bounds_error=False, 
                    fill_value="extrapolate")(tvec).T
        zs = interp1d(Rz_t, zs[enabled_mask], bounds_error=False, 
                    fill_value="extrapolate")(tvec).T
        return tvec, Rs, zs, Trad
    
    def get_data(self, from_to):
        """
        Yields experimental data.
        """
        from_to = np.atleast_2d(from_to)
        rps = []
        datas = []
        for t0, t1 in from_to:
            t_mask = (t0 < self.tvec) * (self.tvec <= t1)
            times = self.tvec[t_mask]
            data = self.Trad[t_mask]
            Rs = self.Rs[t_mask]
            zs = self.zs[t_mask]
            rp = np.empty_like(Rs)
            for i, t in enumerate(times):
                rp[i] = self.equilibrium.cartesian2magnetic(t, np.squeeze(np.dstack((Rs[i], zs[i]))))
            rp = rp.ravel()
            o = rp.argsort()
            rps.append(rp[o])
            datas.append(data.ravel()[o])
        return rps, datas

    def forward_model(self, rho_pols):
        """
        Computes forward model. SimpleECE: Trad = Te
        """
        return interp1d(self.rp_coord.values, unp.nominal_values(self.Te_state.evaluate()),
            bounds_error=False, fill_value="extrapolate")(rho_pols)

    def get_likelihood(self, t0, t1):
        """
        Yields likelihood of model with respect to data.
        """
        r, d = self.get_data((t0, t1))
        fm = self.forward_model(r)
        return self.likelihood.eval(data=unp.nominal_values(d), uncertainty=unp.std_devs(d), model=fm)
