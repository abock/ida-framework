#!/usr/bin/env python

import numpy as np
from ..base_equilibrium import Base2DEquilibrium
import os

class AUG2DEquilibrium(Base2DEquilibrium):
    """docstring for AUG2DEquilibrium."""
    def __init__(self, dd_pulse_number, dd_diag="EQH", dd_experiment="AUGD",
                 dd_edition=0, dd_diag2=None, cache=None):
        tvec, Rvec, zvec, pft, psi_ax, psi_lcfs = self.load_data(
            dd_pulse_number, dd_diag, dd_experiment,
            dd_edition, dd_diag2, cache)

        super(AUG2DEquilibrium, self).__init__(tvec, Rvec, zvec, pft, psi_ax, psi_lcfs)

    def load_data(self, dd_pulse_number, dd_diag, dd_experiment,
                 dd_edition, dd_diag2, cache):
        cachefile = cache+'/aug2dequil_%s_%s_%i_%i.npz'%(dd_experiment, dd_diag,
            dd_pulse_number, dd_edition)
        if cache is None:
            tvec, Rvec, zvec, pft, psi_ax, psi_lcfs = self.load_data_dd(dd_pulse_number,
                dd_diag, dd_experiment, dd_edition, dd_diag2)
        else:
            if os.path.isfile(cachefile):
                print('Found cached data in %s, loading now...'%cachefile)
                data = np.load(cachefile)
                tvec, Rvec, zvec = data['tvec'], data['Rvec'], data['zvec']
                pft, psi_ax, psi_lcfs = data['pft'], data['psi_ax'], data['psi_lcfs']
            else:
                tvec, Rvec, zvec, pft, psi_ax, psi_lcfs = self.load_data_dd(dd_pulse_number,
                    dd_diag, dd_experiment, dd_edition, dd_diag2)
                np.savez(cachefile, tvec=tvec, Rvec=Rvec, zvec=zvec, pft=pft,
                    psi_ax=psi_ax, psi_lcfs=psi_lcfs)
        return tvec, Rvec, zvec, pft, psi_ax, psi_lcfs

    def load_data_dd(self, dd_pulse_number, dd_diag, dd_experiment,
                 dd_edition, dd_diag2):
        #from ddww import dd
        from ..aug import local_dd as dd
        eq_sf = dd.shotfile(dd_diag, dd_pulse_number, dd_experiment, dd_edition,
            dd_diag2)
        tvec = eq_sf.getObjectData('time')
        Rvec = eq_sf.getObjectData('Ri')[0]
        zvec = eq_sf.getObjectData('Zj')[0]
        pft = eq_sf.getObjectData('PFM')
        psi_ax, pfs, pfl = eq_sf('PFxx').data[:len(tvec),:3].T
        psi_lcfs = np.clip(pfs, pfl, None)
        return tvec, Rvec, zvec, pft, psi_ax, psi_lcfs
