#!/usr/bin/env python

import numpy as np
from ..base_equilibrium import Base2DEquilibrium
import os

class ITER2DEquilibrium(Base2DEquilibrium):
    """docstring for ITER2DEquilibrium."""
    def __init__(self, db_pulse_number, db_experiment="ITER",
            db_edition=0, db_input_user='public', cache=None):
        tvec, Rvec, zvec, pft, psi_ax, psi_lcfs = self.load_data(
            db_pulse_number, db_experiment, db_edition, db_input_user, cache)

        super(ITER2DEquilibrium, self).__init__(tvec, Rvec, zvec, pft, psi_ax, psi_lcfs)

    def load_data(self, db_pulse_number, db_experiment,
                 db_edition, db_input_user, cache):
        #cachefile = cache+'/aug2dequil_%s_%s_%i_%i.npz'%(db_experiment, 
        #    db_pulse_number, db_edition, db_input_user)
        #if cache is None:
        tvec, Rvec, zvec, pft, psi_ax, psi_lcfs = self.load_data_dd(db_pulse_number,
            db_experiment, db_edition, db_input_user)
        #else:
        #    if os.path.isfile(cachefile):
        #        print('Found cached data in %s, loading now...'%cachefile)
        #        data = np.load(cachefile)
        #        tvec, Rvec, zvec = data['tvec'], data['Rvec'], data['zvec']
        #        pft, psi_ax, psi_lcfs = data['pft'],
        #        data['psi_ax'], data['psi_lcfs']
        #    else:
        #        tvec, Rvec, zvec, pft, psi_ax, psi_lcfs =
        #        self.load_data_dd(db_pulse_number,
        #            db_experiment, run, db_input_user)
        #        np.savez(cachefile, tvec=tvec, Rvec=Rvec, zvec=zvec, pft=pft,
        #            psi_ax=psi_ax, psi_lcfs=psi_lcfs)

        return tvec, Rvec, zvec, pft, psi_ax, psi_lcfs

    def load_data_dd(self, db_pulse_number, db_experiment, db_edition,
                     db_input_user):
        #from ddww import dd
        #from ..aug import local_dd as dd
        #eq_sf = dd.shotfile(db_pulse_number, db_experiment, db_edition)
        #tvec = eq_sf.getObjectData('time')
        #Rvec = eq_sf.getObjectData('Ri')[0]
        #zvec = eq_sf.getObjectData('Zj')[0]
        #pft = eq_sf.getObjectData('PFM')
        #psi_ax, pfs, pfl = eq_sf('PFxx').data[:len(tvec),:3].T
        #psi_lcfs = np.clip(pfs, pfl, None)

        """
        read equilibrium.
        """       
        import imas

        imas_version                = os.getenv('IMAS_VERSION')[0]     # IMAS DATA VERSION
        #print('IMAS version',imas_version)
        #print(db_experiment, db_pulse_number, db_edition, db_input_user)
        input = imas.DBEntry(imas.imasdef.MDSPLUS_BACKEND,
                             db_experiment, db_pulse_number, db_edition,
                             db_input_user, imas_version)
                             #db_input_user)
        check=input.open()
        #print(check)
        if check[0]!=0:
            raise Exception('Could not open the IMAS file with plasma')

        # !   IF EQUILIBRIUM EXISTS
        try:
            equilibrium = input.get('equilibrium')
            if len(equilibrium.time_slice) == 0:
                    raise RuntimeError('equilibrium IDS is empty: len(equilibrium.time_slice) = 0') 
                    input.close()
        except:
            input.close() 
            raise Exception('Could not open equilibrium IDS')

        # !   IF TIME IS READABLE
        try:
            tvec= input.partial_get(ids_name='equilibrium',data_path='time')
            ntime = len(tvec)
            print('Number of time points = ',ntime)
            print('Shot time from ',tvec[0], 'to', tvec[ntime-1])
        except:
            input.close() 
            raise Exception('Could not read time values')
        
        index = 0
        self.equilibrium = input.get_slice('equilibrium',tvec[index],1)   

        # CHECK COORDINATES IN IDS BEFORE RUNNING THE PHYSICS CODE 
        if len(self.equilibrium.time_slice[0].profiles_2d)>0:
            #Rvec = self.equilibrium.time_slice[0].profiles_2d[0].r
            #zvec = self.equilibrium.time_slice[0].profiles_2d[0].z
            #print(Rvec[:,0])
            #print(zvec[0,:])
            Rvec = equilibrium.time_slice[20].profiles_2d[0].r[:,0]
            zvec = equilibrium.time_slice[20].profiles_2d[0].z[0,:]
            #print(Rvec)
            #print(zvec)
        else:
            raise Exception('Could not read psi coordinates')

        psi_ax   = []
        psi_lcfs = []
        pft      = []
        if ntime>0:
            for i in range(ntime):
                psi_ax.append(equilibrium.time_slice[i].global_quantities.psi_axis)
                psi_lcfs.append(equilibrium.time_slice[i].global_quantities.psi_boundary)
                pft.append(np.transpose(equilibrium.time_slice[i].profiles_2d[0].psi))
                #print(equilibrium.time_slice[i].profiles_2d[0].r[0,0],
                #      equilibrium.time_slice[i].profiles_2d[0].r[50,0],
                #      equilibrium.time_slice[i].profiles_2d[0].z[0,0],
                #      equilibrium.time_slice[i].profiles_2d[0].z[0,91])

            psi_ax = np.array(psi_ax)
            psi_lcfs = np.array(psi_lcfs)
        else:
            raise Exception('Could not read psi axis, lcfs')

        return tvec, Rvec, zvec, pft, psi_ax, psi_lcfs
