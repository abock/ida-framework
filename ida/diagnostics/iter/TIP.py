#!/usr/bin/env python

import argparse,imas,os,copy,pdb,math
import numpy as np
import os
from uncertainties import unumpy as unp
from scipy import interpolate
import xml.etree.ElementTree as ET
from ..base_diagnostic import BaseDiagnostic
from copy import copy

###############################################################
# READ AN XML FILE
###############################################################
def read_xml(filename):
    etree = ET.parse(filename)
    params = etree.getroot()
    dic = {}
    for child in params.getchildren():
        dic[child.tag] = eval(child.text)
    return dic


###############################################################
# ADDITIONAL CLASS FOR INTERNAL CALCULATION
###############################################################
class channel:
    def __init__(self):
        self.name = 'default_channel_name'
        self.LOS_1_point_r = 0.0
        self.LOS_1_point_z = 0.0
        self.LOS_1_point_phi = 0.0
        self.LOS_1_point_x = 0.0
        self.LOS_1_point_y = 0.0
        self.LOS_2_point_r = 0.0
        self.LOS_2_point_z = 0.0
        self.LOS_2_point_phi = 0.0
        self.LOS_2_point_x = 0.0
        self.LOS_2_point_y = 0.0
        self.angle_ZoR = 0.0
        self.LOS_l_vector = []
        self.r_LOS = []
        self.z_LOS = []
        self.phi   = []
        self.x_LOS = []
        self.y_LOS = []
        self.wavelength = 0.0


class SimpleInterferometry(BaseDiagnostic):
    """ ITER Toroidal Interferometer Polarimeter(TIP)."""
    def __init__(self, db_experiment, db_pulse_number, db_edition,
                 db_input_user, input_param_file,
                 rho_pol, ne, equilibrium, likelihood, ne_state_norm = 1e19, ne_data_norm=1e19):

        super(BaseDiagnostic, self).__init__()

        self.db_experiment     = db_experiment
        self.db_pulse_number   = db_pulse_number
        self.db_edition        = db_edition
        self.db_input_user     = db_input_user
        self.input_param_file  = input_param_file

        self.ne_state      = ne
        self.likelihood    = likelihood
        self.ne_state_norm = ne_state_norm
        self.ne_data_norm  = ne_data_norm
        self.rps           = np.linspace(rho_pol.bounds[0], rho_pol.bounds[1], 1000)

        self.equilibrium = equilibrium
        self.equilibrium1 = []          # obsolete if base equilibrium works with time dependent r,z
        self.r_psi = []                 # obsolete if base equilibrium works with time dependent r,z
        self.z_psi = []                 # obsolete if base equilibrium works with time dependent r,z
        self.psi2d = []                 # obsolete if base equilibrium works with time dependent r,z
        self.rho_pol_2d   = []          # obsolete if base equilibrium works with time dependent r,z
        self.psi_axis     = 0           # obsolete if base equilibrium works with time dependent r,z
        self.psi_boundary = 0           # obsolete if base equilibrium works with time dependent r,z
        self.rho_pol_LOS = []

        geometry = self.get_geometry()
        equilibrium1 = self.get_equilibrium()

        f2d = interpolate.RectBivariateSpline(self.r_psi[:,0],self.z_psi[0,:], self.rho_pol_2d)

        for i in range(len(self.ch_geom)):
             nlos = len(self.ch_geom[i].r_LOS)
             self.rho_pol_LOS.append([])
             for j in range(len(self.ch_geom[i].r_LOS)):
                 #self.rho_pol_LOS[i].append(f2d(self.ch_geom[i].r_LOS[j], self.ch_geom[i].z_LOS[j])[0][0])
                 self.rho_pol_LOS[i].append(self.equilibrium.cartesian2magnetic(75.74459999999999, 
                     np.squeeze(np.dstack((self.ch_geom[i].r_LOS[j],
                         self.ch_geom[i].z_LOS[j]))))[0][0])
             self.rho_pol_LOS[i] = np.array(self.rho_pol_LOS[i])


    def get_geometry(self):
        """
        read interferometer geometry.
        """       
        
        #interferometer = imas.interferometer()
        #print(interferometer)

        #input_geometry = imas.DBEntry(imas.imasdef.MDSPLUS_BACKEND,'ITER_MD',150305,1,'public')    # for ITER lokin nodes
        input_geometry = imas.DBEntry(imas.imasdef.MDSPLUS_BACKEND,'iter_md',150305,1,'public')    # for tok0X nodes
        #150305 TIP 150610 DIP
        input_geometry.open()
        interferometer_geometry = input_geometry.get('interferometer')

        # CONSTANTS FROM GEOMETRY INPUT
        if 'TIP' in interferometer_geometry.ids_properties.comment:
            diagnostic='TIP'
        if 'DIP' in interferometer_geometry.ids_properties.comment:
            diagnostic='DIP'

        number_of_channels = len(interferometer_geometry.channel)
        print("get_geometry:", interferometer_geometry.ids_properties.comment)
        alpha              = np.zeros(number_of_channels)

        # AND XML PARAMETERS
        param      = read_xml(self.input_param_file)
        INT_POINTS = param['n_points']
        #NOISE      = param['noise']
    
        # CREATE CHANNELS FOR CALCULATION
        ch_geom = []
        for i in range(number_of_channels):
            ch_geom.append(channel())
            ch_geom[i].name = 'channel_'+str(i+1)
            
            ch_geom[i].LOS_1_point_r   = interferometer_geometry.channel[i].line_of_sight.first_point.r
            ch_geom[i].LOS_1_point_z   = interferometer_geometry.channel[i].line_of_sight.first_point.z
            ch_geom[i].LOS_1_point_phi = interferometer_geometry.channel[i].line_of_sight.first_point.phi
            ch_geom[i].LOS_2_point_r   = interferometer_geometry.channel[i].line_of_sight.second_point.r
            ch_geom[i].LOS_2_point_z   = interferometer_geometry.channel[i].line_of_sight.second_point.z
            ch_geom[i].LOS_2_point_phi = interferometer_geometry.channel[i].line_of_sight.second_point.phi
            ch_geom[i].wavelength      = interferometer_geometry.channel[i].wavelength[0].value

            ch_geom[i].LOS_1_point_x   = ch_geom[i].LOS_1_point_r * math.cos(ch_geom[i].LOS_1_point_phi)
            ch_geom[i].LOS_1_point_y   = ch_geom[i].LOS_1_point_r * math.sin(ch_geom[i].LOS_1_point_phi)
            ch_geom[i].LOS_2_point_x   = ch_geom[i].LOS_2_point_r * math.cos(ch_geom[i].LOS_2_point_phi)
            ch_geom[i].LOS_2_point_y   = ch_geom[i].LOS_2_point_r * math.sin(ch_geom[i].LOS_2_point_phi)
            
            # CALCULATE POINTS OF LOS
            l_R  = np.sqrt(ch_geom[i].LOS_1_point_r**2 + ch_geom[i].LOS_2_point_r**2 - \
                                        2*ch_geom[i].LOS_1_point_r*ch_geom[i].LOS_2_point_r*math.cos(ch_geom[i].LOS_2_point_phi-ch_geom[i].LOS_1_point_phi))
            
            alpha[i] = math.atan(ch_geom[i].LOS_2_point_z - ch_geom[i].LOS_1_point_z)/l_R
            
            ch_geom[i].LOS_l_vector  = np.linspace(0,np.sqrt(l_R**2+(ch_geom[i].LOS_2_point_z\
                                      -ch_geom[i].LOS_1_point_z)**2),INT_POINTS)
            
            cos_b   = (ch_geom[i].LOS_1_point_r**2 + l_R**2 -
                    ch_geom[i].LOS_2_point_r**2)/(2*ch_geom[i].LOS_1_point_r*l_R)
            
            ch_geom[i].r_LOS  = np.sqrt( (math.cos(alpha[i])*ch_geom[i].LOS_l_vector)**2 \
                               + ch_geom[i].LOS_1_point_r**2 \
                               - 2 * ch_geom[i].LOS_1_point_r * ch_geom[i].LOS_l_vector * math.cos(alpha[i])* cos_b ) 
            
            ch_geom[i].z_LOS  = np.linspace(ch_geom[i].LOS_1_point_z,ch_geom[i].LOS_2_point_z, INT_POINTS)
        
            #ch_geom[i].x_LOS  = np.linspace(ch_geom[i].LOS_1_point_x, ch_geom[i].LOS_2_point_x, INT_POINTS)
            #ch_geom[i].y_LOS  = np.linspace(ch_geom[i].LOS_1_point_y, ch_geom[i].LOS_2_point_y, INT_POINTS)

        self.ch_geom = ch_geom

        #with open('tip_los_rz.txt', 'w') as f:
        #    for i in range(number_of_channels):
        #        print("&",file=f)
        #        for j in range(INT_POINTS):
        #            print(ch_geom[i].r_LOS[j],ch_geom[i].z_LOS[j],file=f)

        #with open('tip_los_xy.txt', 'w') as f2:
        #    for i in range(number_of_channels):
        #        print("&",file=f2)
        #        for j in range(INT_POINTS):
        #            print(ch_geom[i].x_LOS[j],ch_geom[i].y_LOS[j],file=f2)

        #with open('tip_los_circles.txt', 'w') as f:
        #    R0   = 6.2
        #    Rmax = R0+2
        #    Rmin = R0-2
        #    print(-Rmax,0,file=f)
        #    print(+Rmax,0,file=f)

        #    print("&",file=f)
        #    print(0,-Rmax,file=f)
        #    print(0,+Rmax,file=f)

        #    print("&",file=f)
        #    for i in range(101):
        #        print(R0*math.sin(i*2*math.pi/100),R0*math.cos(i*2*math.pi/100),file=f)

        #    print("&",file=f)
        #    for i in range(101):
        #        print(Rmax*math.sin(i*2*math.pi/100),Rmax*math.cos(i*2*math.pi/100),file=f)

        #    print("&",file=f)
        #    for i in range(101):
        #        print(Rmin*math.sin(i*2*math.pi/100),Rmin*math.cos(i*2*math.pi/100),file=f)

        #quit("TIP coord")

        return

    def get_equilibrium(self):
        """
        read equilibrium.
        """       
        input = imas.DBEntry(imas.imasdef.MDSPLUS_BACKEND,
                             self.db_experiment, self.db_pulse_number, self.db_edition, self.db_input_user)
        check=input.open()
        if check[0]!=0:
            raise Exception('Could not open the IMAS file with plasma')

        # !   IF EQUILIBRIUM EXISTS
        try:
            equilibrium1 = input.get('equilibrium')
            if len(equilibrium1.time_slice) == 0:
                    raise RuntimeError('equilibrium IDS is empty: len(equilibrium.time_slice) = 0') 
                    input.close()
        except:
            input.close() 
            raise Exception('Could not open equilibrium IDS')

        # !   IF TIME IS READABLE
        try:
            time_array= input.partial_get(ids_name='equilibrium',data_path='time')
            ntime = len(time_array)
            print('Number of time points = ',ntime)
            print('Shot time from ',time_array[0], 'to', time_array[ntime-1])
        except:
            input.close() 
            raise Exception('Could not read time values')
        
        index = 20
        self.equilibrium1 = input.get_slice('equilibrium',time_array[index],1)   
        print("time =",time_array[20])

        # CHECK COORDINATES IN IDS BEFORE RUNNING THE PHYSICS CODE 
        if len(self.equilibrium1.time_slice[0].profiles_2d)>0:
            self.z_psi = self.equilibrium1.time_slice[0].profiles_2d[0].z
            self.r_psi = self.equilibrium1.time_slice[0].profiles_2d[0].r
            #print(self.z_psi[0,:])
            #print(self.r_psi[:,0])
            self.psi_axis     = self.equilibrium1.time_slice[0].global_quantities.psi_axis
            self.psi_boundary = self.equilibrium1.time_slice[0].global_quantities.psi_boundary
        else:
            raise Exception('Could not read psi coordinates')

        if len(self.z_psi)!=len(self.r_psi) or len(self.z_psi)<1:
            raise Exception('Incorrect psi coordinates, check equilibrium')

        try:
            self.psi2d      = self.equilibrium1.time_slice[0].profiles_2d[0].psi #find rho_tor_norm in core_profiles and in eq
            self.rho_pol_2d = np.sqrt((self.psi2d - self.psi_axis) /
                    (self.psi_boundary - self.psi_axis))
            #rho_tor_eq   = self.equilibrium1.time_slice[0].profiles_1d.rho_tor_norm #equilibrium 1d
            #psi1d_eq     = self.equilibrium1.time_slice[0].profiles_1d.psi #psi 1d should be in eq
            #print(rho_tor_eq)
        except:
            raise Exception('Could not read equilibrium')
        # IF NO DATA, NO NEED TO EXECUTE THE PHYSICS CODE

        
    def prepare(self, time_interval, **kwargs):
        """
        Yields experimental data.
        """      
        np.random.seed(123) # data should be noisy, but static
        x = np.linspace(0.1, 0.9, 5)
        y = np.array([4.703218634839091e+20,
                        5.672010776486804e+20,
                        5.1587309685501696e+20,
                        3.0568207364140184e+20,
                        2.0221435520916763e+19])
        
        #y *= (1 + np.random.rand(len(y))*0.2)
        #y = unp.uarray(y, np.clip(np.abs(y)*0.2, 0.1, None))
        y = np.random.normal(y,np.clip(np.abs(y)*0.05, 0.1, None),len(y))
        y = unp.uarray(y, np.clip(np.abs(y)*0.05, 0.1, None))

        self.data_to_return = x, y # data prepared for get_data()
        return super().prepare(time_interval, **kwargs) 

    def get_data(self, apply_ne_norm = True):
        x, y = self.data_to_return
        if apply_ne_norm: y = copy(y) / self.ne_data_norm
        return [x], [y]

    def forward_model(self, rho_pols):
        """
        TIP forward model.
        """
        from ida.diagnostics.iter.iter_equilibrium import ITER2DEquilibrium

        # FOR EACH CHANNEL INTERPOLATE DENSITY ON THE LOS AND INTEGRATE        
        n_line_integral = [] 
        f = interpolate.interp1d(self.rps, 
                                 unp.nominal_values(self.ne_state.evaluate(self.rps)),
                                 bounds_error=False,fill_value="extrapolate")

        rp = np.linspace(0,1.1,100)
        ne = f(rp)
        #with open('ne.rp', 'w') as f1:
        #    for i in range(100):
        #        print(rp[i],ne[i],file=f1)
        
        for i in range(len(self.ch_geom)):
             nlos = len(self.ch_geom[i].r_LOS)
             n_e_LOS = []
             n_e_LOS = f(self.rho_pol_LOS[i])
             n_e_LOS[np.where(n_e_LOS<0)] = 0*n_e_LOS[np.where(n_e_LOS<0)]
        #    if diagnostic=='TIP':        
             LOS=2*max(self.ch_geom[i].LOS_l_vector)
        #    if diagnostic=='DIP':
        #        LOS=max(self.ch_geom[i].LOS_l_vector)
             n_line_integral.append(np.sum(n_e_LOS[1:nlos-1] *
                 (self.ch_geom[i].LOS_l_vector[1:nlos-1] - self.ch_geom[i].LOS_l_vector[0:nlos-2])))
             #print(i,n_line_integral[i])
             n_line_integral[i] = n_line_integral[i]/self.ne_state_norm
    
        #quit("aaaa")

        return [n_line_integral]

    def get_likelihood(self):
        """
        Yields likelihood of model with respect to data.
        """
        r, d = self.get_data()
        fm = self.forward_model(r)
        likeli = self.likelihood.eval(data=unp.nominal_values(d), uncertainty=unp.std_devs(d), model=fm)
        #print("IFY",likeli)
        return likeli

