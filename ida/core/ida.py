#!/usr/bin/env python

import os
import sys
# this is not the cleanest but the easiest way to do reliable sibling imports:
_ = os.path.join(os.path.dirname(__file__), '..')
sys.path.insert(0, _)
from ida.diagnostics.base_diagnostic import BaseDiagnostic
from ida.core.priors import BasePrior
from ida.core.variables import Independent1DVariable, Dependent1DVariable
sys.path.remove(_)

import numpy as np
from scipy.optimize import minimize


class IDA(object):
    """Integrated Data Analysis object. Holds parametrisation of physical system to be analysed."""
    def __init__(self): #, arg):
        super(IDA, self).__init__()
        self.independent_variables = {}
        self.dependent_variables = {}
        self.diagnostics = []
        self.priors = []

    def add_independent_variable(self, iv):
        if isinstance(iv, Independent1DVariable):
            self.independent_variables[iv.name] = iv
        else:
            raise NotImplementedError("Only 1D variables supported so far.")

    def add_dependent_variable(self, dv, *args, **kwargs):
        if isinstance(dv, Dependent1DVariable):
            self.dependent_variables[dv.name] = dv
        else:
            raise NotImplementedError("Only 1D variables supported so far.")

    def get_dependent_variable_name(self, *args, **kwargs):
        """
        Return names of available dependent variables.

        To access all the dependent variables via the interface, the names must be known.
        """
        return [name for name in self.dependent_variables.keys()]

    def get_independent_variable(self, name):
        return self.independent_variables[name]

    def get_dependent_variable(self, name):
        return self.dependent_variables[name]

    def add_diagnostic(self, diag):
        if isinstance(diag, BaseDiagnostic):
            self.diagnostics.append(diag)
        else:
            raise TypeError("Diagnostic not derived from BaseDiagnostic.")

    def add_prior(self, prior):
        if isinstance(prior, BasePrior):
            self.priors.append(prior)
        else:
            raise TypeError("Prior not derived from BasePrior.")

    def pre_opt_workflow(self, *args, **kwargs):
        # for compatibility only
        return self.prepare(*args, **kwargs)

    def prepare(self, time_interval=None, **kwargs):
        self.daps = daps = self.diagnostics + self.priors
        for dap in daps:
            dap.prepare(time_interval, **kwargs)

    def get_starting_parameters(self):
        starting_parameters = []
        for key in self.dependent_variables:
            starting_parameters += list(self.dependent_variables[key].get_parameters())
        return starting_parameters

    def _perform_optimization(self, starting_parameters):
        return minimize(self._get_likelihood, starting_parameters, jac=False,
            #method="BFGS", options={'disp': True},
            method='Nelder-Mead', options={'disp': True, 'adaptive': True} #, 'maxiter':1000},
        )

    def optimize(self):
        self.daps = daps = self.diagnostics + self.priors

        def get_likelihood(parameters):
            for key in self.dependent_variables:
                dv = self.dependent_variables[key]
                dv.set_parameters(parameters[:dv.n_parameters])
                parameters = parameters[dv.n_parameters:]

            return np.sum([dp.get_likelihood() for dp in daps])

        self._get_likelihood = get_likelihood

        print('before:', get_likelihood(self.get_starting_parameters()))
        res = self._perform_optimization(self.get_starting_parameters())
        print('after:', get_likelihood(self.get_starting_parameters()))

        return res

