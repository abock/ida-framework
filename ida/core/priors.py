#!/usr/bin/env python

import numpy as np
from uncertainties import unumpy as unp

class BasePrior(object):
    """BasePrior from which all priors should be derived."""
    def __init__(self):
        super(BasePrior, self).__init__()
        self.t0, self.t1 = None, None

    def prepare(self, time_interval=None, **kwargs):
        """
        Prepare prior object for upcoming optimization.
        """
        if time_interval:
            self.t0, self.t1 = time_interval

    def get_likelihood(self):
        """
        Yields likelihood.
        """
        return np.nan


class Positivity1DPrior(BasePrior):
    """Simple positivity prior."""
    def __init__(self, states, width=0.01, weight=1):
        super(Positivity1DPrior, self).__init__()
        self.states = states
        self.width = width
        self.weight = weight

    def get_likelihood(self):
        """
        Yields likelihood such that negative parameters are penalised. 
        """
        w = np.array(sum([list(unp.nominal_values(s.parameters)) for s in self.states], []))
        return np.sum(self.weight*np.piecewise(w, [w < 0, w >= 0],
            [lambda x: (x/self.width)**2, lambda x: 0]))


class SOLTe1DPrior(BasePrior):
    """ Scrape-Off-Layer maximum Te prior. Only defined for single dependent_variable """
    def __init__(self, state, weight, max_te_sol):
        '''
        Constructor
        '''
        raise NotImplementedError('this needs to be re-thought for dynamic axis values')
        super(SOLTe1DPrior, self).__init__(state, weight)
        self.max_te_sol = max_te_sol
        
    def get_likelihood(self):
        prior = 0
        for dependent_variable in self.state.values():
            axis, values = dependent_variable.get_axis_and_values()
            prior += self.weight*np.sum(
                     values[np.logical_and(values > self.max_te_sol, 
                                           axis > 1.0)]) / axis.size
        return prior

    
class Monotonicity1DPrior(BasePrior):
    """Prior to penalize non-monotonic behaviour."""
    def __init__(self, state, weight, prior_decay, rho_start, rho_stop):
        raise NotImplementedError('this needs to be re-thought for dynamic axis values')
        super(Monotonicity1DPrior, self).__init__(state, weight)
        self.prior_decay = prior_decay
        self.rho_start = rho_start
        self.rho_stop = rho_stop

    def get_likelihood(self):
        prior = 0
        for dependent_variable in self.state.values():
            axis, derivative = dependent_variable.get_axis_and_values(dn=1)
            mask = np.logical_and(axis > self.rho_start, axis < self.rho_stop)
            derivative = derivative[mask]
            prior += self.weight*np.sum(np.arctan(derivative/self.prior_decay) + np.pi / 2.0)
        return prior


class Curvature1DPrior(BasePrior):
    """Prior to penalize excessive curvature."""
    def __init__(self, state, weight, rho_1, rho_1_decay, rho_1_scale, \
                 rho_2, rho_2_decay, rho_2_scale):
        raise NotImplementedError('this needs to be re-thought for dynamic axis values')
        super(Curvature1DPrior, self).__init__(state, weight)
        self.rho_1 = rho_1
        self.rho_1_decay = rho_1_decay
        self.rho_1_scale = rho_1_scale
        self.rho_2 = rho_2
        self.rho_2_decay = rho_2_decay
        self.rho_2_scale = rho_2_scale

    def get_likelihood(self):
        prior = 0
        for dependent_variable in self.state.values():
            axis, curv = dependent_variable.get_axis_and_values(dn=1)
            scaling = np.zeros(axis.shape)
            scaling[:] = self.weight
            scaling[axis < self.rho_1] *= (
                    (self.rho_1_scale + (1.0 - self.rho_1_scale) *
                    np.exp( (axis[axis < self.rho_1] - self.rho_1)
                    / self.rho_1_decay)))
            scaling[axis > self.rho_2] *= (
                    (self.rho_2_scale + (1.0 - self.rho_2_scale) *
                     np.exp(-(axis[axis > self.rho_2] - self.rho_2)
                    / self.rho_2_decay)))
            prior += np.sum(curv**2 / scaling**2) / axis.size**2
        return prior