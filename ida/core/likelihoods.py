#!/usr/bin/env python

import numpy as np

class Likelihood(object):
    """Base class from which all likelihoods should be derived."""
    def __init__(self):
        super(Likelihood, self).__init__()

    def eval(self, parameter_list):
        """Yields log likelihood."""
        pass


class GaussianLikelihood(Likelihood):
    def __init__(self):
        super(GaussianLikelihood, self).__init__()

    def eval(self, data, uncertainty, model):
        return np.sum(((data - np.atleast_2d(model)) / uncertainty)**2)


class CauchyLikelihood(Likelihood):
    def __init__(self, a0=None):
        super(CauchyLikelihood, self).__init__()
        if a0 is not None:
            self.a0 = a0
        
    def eval(self, data, uncertainty, model):
        return (self.a0 + 0.5) * np.sum(np.log( 2.0 * self.a0
                + ((data - np.atleast_2d(model)) / uncertainty)**2))
