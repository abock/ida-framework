#!/usr/bin/env python

import numpy as np
from scipy.interpolate import CubicSpline
from .exceptions import OutOfBoundsException

import logging  # should already be initialised in calling module
logger = logging.getLogger("variables")

class Independent1DVariable(object):
    def __init__(self, name, bounds, *args, **kwargs):
        super(Independent1DVariable, self).__init__(*args, **kwargs)
        self.name = name
        self.bounds = bounds


class BoundedIndependent1DVariable(Independent1DVariable):
    def __init__(self, name, bounds, *args, **kwargs):
        super(BoundedIndependent1DVariable, self).__init__(name, bounds, *args, **kwargs)

    def check_all(self, values):
        if(np.any(values < self.bounds[0]) or np.any(values > self.bounds[1])):
            raise OutOfBoundsException(self, self.bounds, values)

    def reduce(self, values, inclusive=True):
        return values[self.mask(values, inclusive=inclusive)]

    def mask(self, values, inclusive=True):
        if(inclusive):
            return np.logical_and(values >= self.bounds[0], \
                                  values <= self.bounds[1])
        else:
            return np.logical_and(values > self.bounds[0], \
                                  values < self.bounds[1])


class DependentVariable(object):
    def __init__(self, name, *args):
        super(DependentVariable, self).__init__(*args)
        self.name = name

    def evaluate(self, args):
        return None


class Dependent0DVariable(DependentVariable):
    def __init__(self, name, parameter=0.0, *args):
        super(Dependent1DVariable, self).__init__(name, *args)
        self.parameter = parameter


class Dependent1DVariable(DependentVariable):
    """
    Dependent one-dimensional variables, e.g. density in rho_pol.

    Contains a set of values:
        - knots: knot/support location
        - n_parameters: number of parameters of the model
        - parameters: a local store for the last set parameters
        - unit: the pure SI unit of the quantity that is represented
        - scale: the expected scale of the quantity.
          Note: For plotting on an axis with scale 1, the output of the value is
                divided by (scale * unit), e.g. 1e3 eV = keV.
                So for core temperatures the scale is 1e3 and the unit eV.
    """
    def __init__(self, name, independent_variable, knots, unit, scale=1.0, *args):
        super(Dependent1DVariable, self).__init__(name, *args)
        self.independent_variable = independent_variable
        self.knots = knots
        self.n_parameters = len(knots)
        self.parameters = np.zeros(self.n_parameters)
        self.unit = unit
        self.scale = scale

    def set_parameters(self, parameters):
        """
        Check and store parameters internally and set internal coefficients.

        For each new set of parameters the number of elements is checked,
        it is made available as member variable, and if needed the model
        is set up once to speed up evaluation later on.
        """
        if len(parameters) != len(self.parameters):
            raise ValueError("The number of parameters must not be changed.")
        self.parameters = parameters

    def get_parameters(self):
        """ Return internally saved parameter from the last call. """
        return self.parameters

    def suggest_parameters(self, user_parameters):
        """
        The model is not necessarily linear, so let the user suggest parameter corresponding
        to the function values, while this method translates it into usable parameters.
        """
        return user_parameters


class CubicSplineDependent1DVariable(Dependent1DVariable):
    def __init__(self, name, independent_variable, knots, unit, scale=1.0, *args):
        super(CubicSplineDependent1DVariable, self).__init__(
            name, independent_variable, knots, unit, scale, *args)
        self.basis_functions = []
        for cv in np.eye(len(self.knots)):
            func = CubicSpline(self.knots, cv, bc_type=('clamped', 'clamped'), extrapolate=True)
            self.basis_functions.append(func)

    def evaluate(self, x, dn=0):
        return np.sum([p*bf.derivative(dn)(x) for p, bf in zip(self.parameters, self.basis_functions)], axis=0)


class ExpCubicSplineDependent1DVariable(CubicSplineDependent1DVariable):
    """Reparameterisation of CubicSpline, acting as spline(exp(parameters))."""

    def __init__(self, *args):
        super(ExpCubicSplineDependent1DVariable, self).__init__(*args)

    def evaluate(self, x, dn=0):
        return np.sum([np.exp(p)*bf.derivative(dn)(x) for p, bf in zip(self.parameters, self.basis_functions)], axis=0)


# TODO: rename this class, as the same cubic spline is used as above, but the implementation is more efficient.
class UnivSplineDependent1DVariable(Dependent1DVariable):
    """ Alternative cubic spline evaluation, initialising the spline for each call. """

    def __init__(self, name, independent_variable, knots, unit, scale=1.0, *args):
        super(UnivSplineDependent1DVariable, self).__init__(name, independent_variable, knots, unit, scale, *args)
        self.SplineBasis = CubicSpline

    def set_parameters(self, parameters):
        """
        Check and store parameters internally and set spline coefficients.

        For each new set of parameters the number of elements is checked,
        it is made available as member variable, and spline coefficients are
        calculated with the derivatives at the boundary set to zero.
        """
        if len(parameters) != len(self.parameters):
            raise ValueError("The number of parameters must not be changed.")
        self.parameters = parameters
        self.UnivInst = self.SplineBasis(self.knots, self.parameters, bc_type=('clamped', 'clamped'))

    def evaluate(self, x, dn=0):
        """ Evaluate the spline or its gradient at a given position. """
        if dn == 0:
            return self.UnivInst(x) * self.scale
        else:
            return self.UnivInst.derivative(dn)(x) * self.scale

    def suggest_parameters(self, user_parameters):
        """
        Model is linear, but takes the scale out of the parameters, to they are ~1
        """
        return user_parameters / self.scale


class ExpUnivSplineDependent1DVariable(UnivSplineDependent1DVariable):
    """ Exponential of cubic spline: exp(spline(parameters)). """

    def __init__(self, *args, **kwargs):
        super(ExpUnivSplineDependent1DVariable, self).__init__(*args, **kwargs)

    def evaluate(self, x, dn=0):
        """
        Evaluate the spline or its gradient at a given position.

        Note: Only first and second derivative are supported.
        """
        if dn == 0:
            return np.exp(self.UnivInst(x)) * self.scale
        elif dn == 1:
            return np.exp(self.UnivInst(x)) * self.UnivInst.derivative(1)(x) * self.scale
        elif dn == 2:
            return np.exp(self.UnivInst(x)) * ( \
                self.UnivInst.derivative(1)(x)**2 + self.UnivInst.derivative(2)(x)) * self.scale
        # ignoring higher derivatives: cubic spline is pice wise constant then...
        else:
            raise ValueError("ExpUnivSpline got too high derivative order '{}'".format(dn))

    def suggest_parameters(self, user_parameters):
        """
        Model output is transformed as "exp(parameter) * scale", so use log(user_parameters) / scale.

        For security use max(1e-10, user_parameter) as input for logarithm.
        """
        lower_limit = 1e-2
        if np.any(user_parameters < lower_limit):
            # Version A: just apply lower limit
            # logger.warning("user_parameter for suggest_parameters was < {:.1e}: {}".format(
            #     lower_limit, np.where(user_parameters < lower_limit)[0]))
            # logger.warning("setting them to this lower limit.")
            # logger.warning("Note: exp(spline) model is strictly non-negative!")
            # user_parameters = np.maximum(user_parameters, lower_limit)
            # Version B: set them all to the lowest valid value halved
            indices = (user_parameters < lower_limit)
            user_parameters = user_parameters * 1
            user_parameters[indices] = np.min(user_parameters[~indices]) / 2
        return np.log(user_parameters / self.scale)
