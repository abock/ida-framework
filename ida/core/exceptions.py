class OutOfBoundsException(Exception):
    """Will be raised if something is set outside of the defined bounds """
    def __init__(self, origin,  bounds, values):
        self.origin_name = origin.name
        self.bounds = bounds
        self.values = values