import numpy as np
from uncertainties import unumpy as unp
from scipy.interpolate import interp1d

class SampleECEForwardModel(object):
    """
    This class is an example of distributable forward model
    that can be shared by diagnostic objects.
    Since forward models do not interface directly with the 
    wider IDA core, they do not need to adhere to any particular form
    as long as the corresponding diagnostic developers are informed.
    """
    def __init__(self, Te_state, rho_pols, Te_norm = 1):
        self.Te_state = Te_state
        self.rho_pols = rho_pols
        self.Te_norm = Te_norm
    
    def prepare(self, *args, **kwargs):
        """This could be used to prepare more complex calculations beforehand."""
        pass

    def eval(self, rhos=None, **kwargs):
        if rhos:
            return interp1d(self.rho_pols, self.Te_state.evaluate(self.rho_pols) / self.Te_norm, **kwargs)(rhos)
        else:
            return self.Te_state.evaluate(self.rho_pols) / self.Te_norm

    def mask_measurements(self, data):
        """
        Returns a mask removing bad data points.
        Can either operate based on the state or
        based on the data set itself. The mask
        will also be stored locally to adapt the
        shape of the synthetic measurements.
        """
        return np.ones(unp.nominal_values(data.shape), dtype=np.bool)
