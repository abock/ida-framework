# ITER IDA

The code IDA is designed to estimate electron temperature and density
profiles from a multi-diagnostics suite (Interferometry, TS, ECE, ..) 
The code is written in Python.

## Build status and test coverage

none

## Installation and execution

The execution depends on the platform. Tested on the ITER platform and
on the ASDEX Upgrade linux cluster (e.g. tok01).

ASDEX Upgrade linux cluster (tok01):
```
#clone the repository
git clone https://git.iter.org/scm/an/ida-framework.git
cd ida-framework

#environment setting
module purge
module load git intel imas-modules/1.0 mdsplus/7.84.8 hdf5-serial/1.8.21 boost/1.74 IMAS/3.32.1-4.9.1 anaconda/3/2019.03
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${HDF5_HOME}/lib
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${BOOST_ROOT}/lib

#master branch
git checkout master
cd sample_workflows

#fischerr branch
git checkout fischerr
cd sample_workflows

#run the code
python run_iter_sample.py
```

ITER platform:
```
ssh -X username@hpc-login.iter.org
ssh -X sdcc-login01.iter.org (nur nach hpc-login)

#clone the repository
git clone https://git.iter.org/scm/an/ida-framework.git
cd ida-framework

#environment setting
module purge
module load IMAS
module load uncertainties
module load IPython

#master branch
git checkout master
cd sample_workflows

#fischerr branch
git checkout fischerr
cd sample_workflows

#run the code
python run_iter_sample.py
```

## Dependencies and software requirements:

## Contributors


Following is a running list of contributors:

1. [Dr. Rainer Fischer](https://www.ipp.mpg.de), IPP

2. [Dr. Alexander Bock](https://www.ipp.mpg.de), IPP

3. Dr. Severin Denk, MIT

## Documentation and References

### For the methodology, implementation and validation, please refer to 

* Integrated data analysis of profile diagnostics at ASDEX Upgrade.
R. Fischer et al., [Fusion Science & Technology, 58 675-684 (2010)](http://dx.doi.org/10.13182/FST10-110). 

* Analysis of electron cyclotron emission with extended electron cyclotron forward modeling.
S. Denk et al., [Plasma Phys.\ Control.\ Fusion, 60 105010 (2018)](http://dx.doi.org/10.1088/1361-6587/aadb2f). 

## Contact

If you have any questions, comments or suggestions for improvements, please
contact Rainer Fischer.
